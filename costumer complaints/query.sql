-- jumlah complaint setiap bulan
select
	YEAR(`Date Received`) as tahun,
	MONTH(`Date Received`) as bulan,
	COUNT(`Date Received`) as jumlah
from `ConsumerComplaints` group by YEAR(`Date Received`), MONTH(`Date Received`)
order by YEAR(`Date Received`), MONTH(`Date Received`);

--complaint ber tag older americas
select * from `ConsumerComplaints`
where Tags like '%Older American%';


-- view jumlah costumer complain
create view v_company_response as
    select
Company,
SUM(
    case when `Company Public Response` = 'Closed'
    then 1
    else 0
    end
    ) as Closed,
    sum(
        case
            when `Company Response to Consumer` = 'Closed with explanation'
        then 1
        else 0
        end
    ) as 'Closed with explanation',
    sum(
        case
        when`Company Response to Consumer` = 'Closed with non-monetary relief'
        then 1
        else 0
        end
    ) as 'Consumer Complaints'
from `ConsumerComplaints`
group by Company
order by Company